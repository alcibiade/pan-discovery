package org.alcibiade.pandiscovery.fs;

import org.alcibiade.pandiscovery.fs.scan.FileScanningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Scanning service implementation.
 */
@Component
public class ScanningService {
    private Logger logger = LoggerFactory.getLogger(ScanningService.class);

    private final FileScanningService fileScanningService;

    @Autowired
    public ScanningService(FileScanningService fileScanningService) {
        this.fileScanningService = fileScanningService;
    }

    public void scan(List<String> paths) {
        for (String pathString : paths) {
            logger.info("Processing {}", pathString);

            Path path = Paths.get(pathString);
            try (ScanningFileVisitor visitor = new ScanningFileVisitor(fileScanningService)) {
                Files.walkFileTree(path, visitor);
            } catch (IOException | InterruptedException e) {
                logger.error("Failed to process path " + pathString, e);
            }
        }
    }
}
