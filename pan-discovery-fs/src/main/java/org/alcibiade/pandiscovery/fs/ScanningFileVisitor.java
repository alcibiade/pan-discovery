package org.alcibiade.pandiscovery.fs;

import org.alcibiade.pandiscovery.fs.scan.FileScanningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ScanningFileVisitor implements FileVisitor<Path>, AutoCloseable {
    private Logger logger = LoggerFactory.getLogger(ScanningFileVisitor.class);
    private ExecutorService scanExecutor =
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    private FileScanningService fileScanningService;

    public ScanningFileVisitor(FileScanningService fileScanningService) {
        this.fileScanningService = fileScanningService;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
        scanExecutor.execute(() -> fileScanningService.scan(file));
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) {
        logger.debug("Exception on file " + file, exc);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public void close() throws InterruptedException {
        this.scanExecutor.shutdown();
        this.scanExecutor.awaitTermination(1, TimeUnit.HOURS);
    }
}
